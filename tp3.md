

## I. Setup GNS3

🌞 **Mettre en place la topologie dans GS3**
```bash
[rocky@node1net1tp3 ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=5.38 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=2.30 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=1.01 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=1.67 ms
64 bytes from 10.3.1.12: icmp_seq=5 ttl=64 time=1.18 ms
^C
--- 10.3.1.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4008ms
rtt min/avg/max/mdev = 1.010/2.306/5.377/1.598 ms

[rocky@node2net1tp3 ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.21 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=1.06 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=1.53 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=0.998 ms
^C
--- 10.3.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 0.998/1.199/1.530/0.206 ms
```
```
[rocky@node1net2tp3 ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=4.62 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=64 time=1.13 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=64 time=1.15 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=64 time=1.08 ms
^C
--- 10.3.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 1.083/1.994/4.619/1.515 ms

[rocky@node2net2tp3 ~]$ ping 10.3.2.11
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=64 time=1.54 ms
64 bytes from 10.3.2.11: icmp_seq=2 ttl=64 time=1.12 ms
64 bytes from 10.3.2.11: icmp_seq=3 ttl=64 time=0.866 ms
64 bytes from 10.3.2.11: icmp_seq=4 ttl=64 time=1.18 ms
^C
--- 10.3.2.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 0.866/1.176/1.540/0.240 ms
```
```
[rocky@routeurnet1tp3 ~]$ ping 10.3.100.2
PING 10.3.100.2 (10.3.100.2) 56(84) bytes of data.
64 bytes from 10.3.100.2: icmp_seq=1 ttl=64 time=1.11 ms
64 bytes from 10.3.100.2: icmp_seq=2 ttl=64 time=0.997 ms
64 bytes from 10.3.100.2: icmp_seq=3 ttl=64 time=1.09 ms
64 bytes from 10.3.100.2: icmp_seq=4 ttl=64 time=1.00 ms
^C
--- 10.3.100.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 0.997/1.048/1.108/0.050 ms

[rocky@routeurnet2tp3 ~]$ ping 10.3.100.1
PING 10.3.100.1 (10.3.100.1) 56(84) bytes of data.
64 bytes from 10.3.100.1: icmp_seq=1 ttl=64 time=2.57 ms
64 bytes from 10.3.100.1: icmp_seq=2 ttl=64 time=0.911 ms
64 bytes from 10.3.100.1: icmp_seq=3 ttl=64 time=0.900 ms
64 bytes from 10.3.100.1: icmp_seq=4 ttl=64 time=1.42 ms
^C
--- 10.3.100.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3003ms
rtt min/avg/max/mdev = 0.900/1.451/2.572/0.680 ms
```
```
[rocky@routeurnet1tp3 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=128 time=15.4 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=128 time=13.9 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=128 time=15.9 ms
^C
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 13.877/15.030/15.859/0.841 ms
[rocky@routeurnet1tp3 ~]$ ping google.com
PING google.com (172.217.20.174) 56(84) bytes of data.
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=1 ttl=128 time=15.6 ms
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=2 ttl=128 time=14.9 ms
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=3 ttl=128 time=17.0 ms
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=4 ttl=128 time=14.7 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 14.707/15.531/16.962/0.883 ms
[rocky@routeurnet1tp3 ~]$
```

## II. Routes routes routes

🌞 **Activer le routage sur les deux machines `router`**

```bash
[rocky@routeurnet1tp3 ~]$ sudo sysctl -w net.ipv4.ip_forward=1
[sudo] password for rocky:
net.ipv4.ip_forward = 1
[rocky@routeurnet1tp3 ~]$ sudo firewall-cmd --add-masquerade
success
[rocky@routeurnet1tp3 ~]$ sudo firewall-cmd --add-masquerade --permanent
success
[rocky@routeurnet1tp3 ~]$
```

🌞 **Mettre en place les routes locales**
```
[rocky@node1net1tp3 ~]$ ping 10.3.2.11
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=62 time=6.22 ms
64 bytes from 10.3.2.11: icmp_seq=2 ttl=62 time=4.56 ms
64 bytes from 10.3.2.11: icmp_seq=3 ttl=62 time=3.96 ms
^C
--- 10.3.2.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 3.964/4.915/6.224/0.956 ms
[rocky@node1net1tp3 ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=62 time=8.98 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=62 time=3.85 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=62 time=3.72 ms
^C
--- 10.3.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 3.722/5.516/8.979/2.448 ms
```
```
[rocky@node1net2tp3 ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=62 time=3.36 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=62 time=4.35 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=62 time=4.47 ms
^C
--- 10.3.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 3.362/4.062/4.470/0.497 ms
[rocky@node1net2tp3 ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=62 time=4.07 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=62 time=8.50 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=62 time=4.52 ms
^C
--- 10.3.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 4.074/5.696/8.499/1.989 ms
```


🌞 **Mettre en place les routes par défaut**
```
[rocky@node1net1tp3 ~]$ ping google.com
PING google.com (172.217.20.206) 56(84) bytes of data.
64 bytes from waw02s08-in-f206.1e100.net (172.217.20.206): icmp_seq=1 ttl=127 time=30.5 ms
64 bytes from waw02s08-in-f206.1e100.net (172.217.20.206): icmp_seq=2 ttl=127 time=28.5 ms
64 bytes from waw02s08-in-f206.1e100.net (172.217.20.206): icmp_seq=3 ttl=127 time=28.3 ms
64 bytes from waw02s08-in-f206.1e100.net (172.217.20.206): icmp_seq=4 ttl=127 time=29.0 ms
64 bytes from waw02s08-in-f206.1e100.net (172.217.20.206): icmp_seq=5 ttl=127 time=27.0 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4005ms
rtt min/avg/max/mdev = 27.023/28.669/30.482/1.124 ms
```
```
[rocky@node2net1tp3 ~]$ ping google.com
PING google.com (172.217.20.174) 56(84) bytes of data.
64 bytes from waw02s07-in-f174.1e100.net (172.217.20.174): icmp_seq=1 ttl=127 time=29.4 ms
64 bytes from par10s49-in-f14.1e100.net (172.217.20.174): icmp_seq=2 ttl=127 time=29.4 ms
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=3 ttl=127 time=30.0 ms
64 bytes from waw02s07-in-f174.1e100.net (172.217.20.174): icmp_seq=4 ttl=127 time=29.4 ms
64 bytes from par10s49-in-f14.1e100.net (172.217.20.174): icmp_seq=5 ttl=127 time=31.2 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 29.392/29.883/31.165/0.679 ms
```
```
[rocky@node1net2tp3 ~]$ ping google.com
PING google.com (172.217.20.174) 56(84) bytes of data.
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=1 ttl=126 time=31.5 ms
64 bytes from waw02s07-in-f174.1e100.net (172.217.20.174): icmp_seq=2 ttl=126 time=31.0 ms
64 bytes from par10s49-in-f14.1e100.net (172.217.20.174): icmp_seq=3 ttl=126 time=30.8 ms
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=4 ttl=126 time=34.3 ms
64 bytes from waw02s07-in-f174.1e100.net (172.217.20.174): icmp_seq=5 ttl=126 time=31.8 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 30.846/31.872/34.272/1.245 ms
```
```
[rocky@node2net2tp3 ~]$ ping google.com
PING google.com (172.217.20.174) 56(84) bytes of data.
64 bytes from par10s49-in-f14.1e100.net (172.217.20.174): icmp_seq=1 ttl=126 time=35.2 ms
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=2 ttl=126 time=31.6 ms
64 bytes from waw02s07-in-f174.1e100.net (172.217.20.174): icmp_seq=3 ttl=126 time=29.7 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 29.706/32.199/35.243/2.293 ms
[rocky@node2net2tp3 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=126 time=44.9 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=126 time=55.5 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 44.899/50.182/55.466/5.283 ms
```
# I. Serveur dhcp

🌞 **Setup de la machine dhcp.net1.tp3**

node2net1.tp3 (qui sera le dhcp) :
```
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
# create new
# specify domain name
option domain-name     "net1.tp3";
# specify DNS server's hostname or IP address
option domain-name-servers     1.1.1.1;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    # specify gateway
    option routers 10.3.1.254;
}
```
🌞 **Preuve !**

node1net1.tp3 :

```
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes
```
```
[rocky@node1net1tp3 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1a:6c:1e brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 478sec preferred_lft 478sec
    inet6 fe80::a00:27ff:fe1a:6c1e/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d2:34:3f brd ff:ff:ff:ff:ff:ff
    inet 192.168.214.6/24 brd 192.168.214.255 scope global dynamic noprefixroute enp0s8
       valid_lft 377sec preferred_lft 377sec
    inet6 fe80::4a9c:f6cb:5973:6752/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
```
[rocky@node1net1tp3 ~]$ ip r s
default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.50 metric 102
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.50 metric 102
192.168.214.0/24 dev enp0s8 proto kernel scope link src 192.168.214.6 metric 101
[rocky@node1net1tp3 ~]$
```
```
[rocky@node1net1tp3 ~]$ ping google.com
PING google.com (142.250.75.238) 56(84) bytes of data.
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=1 ttl=127 time=9.04 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=2 ttl=127 time=7.61 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=3 ttl=127 time=8.53 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 7.608/8.392/9.037/0.591 ms
[rocky@node1net1tp3 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=127 time=9.65 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=127 time=11.0 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=127 time=9.99 ms
^C
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 9.653/10.215/11.006/0.575 ms
[rocky@node1net1tp3 ~]$
```
# II. Serveur Web

node1net2 sera le serveur web
```
[rocky@node1net2tp3 ~]$ sudo mkdir /var/www
[rocky@node1net2tp3 ~]$ sudo mkdir /var/www/efrei_site_nul
[rocky@node1net2tp3 ~]$ sudo chown nginx:nginx /var/www/efrei_site_nul
[rocky@node1net2tp3 ~]$ echo "Coucou EFREI" | sudo tee /var/www/efrei_site_nul/index.html
Coucou EFREI
[rocky@node1net2tp3 ~]$ sudo chown nginx:nginx /var/www/efrei_site_nul/index.html
[rocky@node1net2tp3 ~]$ sudo nano /etc/nginx/conf.d/web.net2.tp3.conf
[rocky@node1net2tp3 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
```
```
[rocky@node1net2tp3 ~]$ sudo chown nginx:nginx /var/www/efrei_site_nul/index.html
[rocky@node1net2tp3 ~]$ sudo nano /etc/nginx/conf.d/web.net2.tp3.conf
[rocky@node1net2tp3 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for rocky:
success
[rocky@node1net2tp3 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
```
[rocky@node1net2tp3 ~]$ sudo systemctl start nginx
[rocky@node1net2tp3 ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[rocky@node1net2tp3 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; preset: disabled)
     Active: active (running) since Sat 2023-10-28 16:22:59 CEST; 13s ago
   Main PID: 11552 (nginx)
      Tasks: 2 (limit: 4612)
     Memory: 2.0M
        CPU: 27ms
     CGroup: /system.slice/nginx.service
             ├─11552 "nginx: master process /usr/sbin/nginx"
             └─11553 "nginx: worker process"
```
```
[rocky@node1net2tp3 ~]$ curl http://10.3.2.11
Coucou efrei
```
Depuis node1net1 : 
```
[rocky@node1net1tp3 ~]$ curl http://10.3.2.11
Coucou efrei
```
```
[rocky@node1net2tp3 ~]$ ping http://web.net2.tp3
PING http://web.net2.tp3 (10.3.2.11) 56(84) bytes of data.
64 bytes from http://web.net2.tp3 (10.3.2.11): icmp_seq=1 ttl=64 time=0.046 ms
64 bytes from http://web.net2.tp3 (10.3.2.11): icmp_seq=2 ttl=64 time=0.061 ms
64 bytes from http://web.net2.tp3 (10.3.2.11): icmp_seq=3 ttl=64 time=0.059 ms
^C
--- http://web.net2.tp3 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2035ms
rtt min/avg/max/mdev = 0.046/0.055/0.061/0.006 ms
```
